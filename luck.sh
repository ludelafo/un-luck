#!/usr/bin/env bash

## Error handling
set -o errexit
set -o pipefail
set -o noclobber
set -o nounset
set -o functrace

## Switch to the directory where the script is executed
cd "$(dirname "$(realpath "$0")")";

## Load configuration
source un-luck.conf

## Script
echo "Unmounting the LUKS mapper mount point (${LUKS_MAPPER_MOUNT_POINT})..."

umount "${LUKS_MAPPER_MOUNT_POINT}"

echo "Locking the partition (/dev/disk/by-uuid/${ENCRYPTED_PARTITION_UUID})..."

cryptsetup luksClose "${LUKS_MAPPER_NAME}"

echo "Removing the mount point (${LUKS_MAPPER_MOUNT_POINT})..."

rm -r "${LUKS_MAPPER_MOUNT_POINT}"

sync

echo "The encrypted partition is unmounted and locked!"
