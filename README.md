# (un)luck

My personal scripts for (un)locking my drives encypted with [LUKS](https://gitlab.com/cryptsetup/cryptsetup/blob/master/README.md).

## Usage

You can change the configuration in the [`un-luck.conf`](./un-luck.conf) file.

### `unluck.sh`

This script allows to unlock a partition encrypted with LUKS.

```sh
# Change the permissions to execute the script
chmod +x unluck.sh

# Execute the script
sudo bash unluck.sh
```

### `luck.sh`

This script allows to lock a partition encrypted with LUKS previously mounted with the `unluck.sh` script.

```sh
# Change the permissions to execute the script
chmod +x luck.sh

# Execute the script
sudo bash luck.sh
```

## License

The project is licensed under the MIT License - see the [`LICENSE`](./LICENSE) file for details.
