#!/usr/bin/env bash

## Error handling
set -o errexit
set -o pipefail
set -o noclobber
set -o nounset
set -o functrace

## Switch to the directory where the script is executed
cd "$(dirname "$(realpath "$0")")";

## Load configuration
source un-luck.conf

## Script
echo "Unlocking the partition (/dev/disk/by-uuid/${ENCRYPTED_PARTITION_UUID})..."

cryptsetup luksOpen "/dev/disk/by-uuid/${ENCRYPTED_PARTITION_UUID}" "${LUKS_MAPPER_NAME}"

echo "Creating the mount point (${LUKS_MAPPER_MOUNT_POINT})..."

mkdir -p "${LUKS_MAPPER_MOUNT_POINT}"

echo "Mounting the LUKS mapper on the mount point (${LUKS_MAPPER_MOUNT_POINT})..."

mount "/dev/mapper/${LUKS_MAPPER_NAME}" "${LUKS_MAPPER_MOUNT_POINT}"

sync

echo "The encrypted partition is unlocked and mounted!"
